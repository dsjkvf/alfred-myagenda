MyAgenda
========

## About

MyAgenda is a [workflow](https://www.alfredapp.com/help/workflows/) for [Alfred 2](https://www.alfredapp.com/), which uses [iCalBuddy](http://hasseg.org/icalBuddy/) at its core to display [current] events and reminders on macOS. MyAgenda is also mostly a simplified version of [agenda workflow](https://github.com/jmjeong/alfred-extension/tree/master/agenda) by [Jaemok Jeong](https://github.com/jmjeong/), and therefore all the credits and thanks should go directly to him.

This repository contains only the modified version of the Python script (`agn.py`), which makes use of the `iCalBuddy` program and produces the output needed by `Alfred 2`. The bundled workflow can be downloaded [here](https://bitbucket.org/dsjkvf/alfred-myagenda/downloads/MyAgenda.alfredworkflow).

## Usage

MyAgenda comes with 4 predifined shortcuts:

  1. `Ctrl-Shift-L`: display events that are **l**eft for today;
  2. `Ctrl-Shift-E`: display the **e**ntirety of today's events and reminders;
  3. `Ctrl-Shift-O`: display the sc**o**pe of events and reminders for two weeks starting from tomorrow.
  4. `Ctrl-Shift-T`: display reminders (or **t**asks) that have no due date;

Which is achived via the following parameters passed to the `agn.py` script:

  1. no additional parameter; this is a default action;
  2. `0`, which means 0 additional days;
  3. `13`, which means 13 additional days;
  4. `t`, which [once again] means `tasks`.

Obviously, you may very well prefer not to use shortcuts at all but just toggle `Alfred`'s window, type `agn` to see what's left for today, or to type `agn1` to see your agenda for today and tomorrow, or to type `agnt` to see those of your reminders that have no due date set.
