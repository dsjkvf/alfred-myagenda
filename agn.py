#!/usr/bin/env python
# -*- coding: utf-8 -*-

# HEADER

# Details
__title__ = ""
__author__ = "dsjkvf"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__copyright__ = "dsjkvf"
__credits__ = []
__license__ = "GPL"
__version__ = "1.0.5"
__status__ = "Production"

# MODULES

# System
import sys
import subprocess
import datetime
import pytz

# GLOBAL VARIABLES

# Options
# shift from GTM
if bool(datetime.datetime.now(pytz.timezone('Europe/Brussels')).dst()):
    gmt_shift = '+0300'
else:
    gmt_shift = '+0200'
# the calendars to exclude
cal_excl = ['Man Utd', 'Holidays']
# include today in future agenda or not
today_in = False
# the core command line
buddy_cmd = "./iCalBuddy -nrd -std -tf '%H:%M' "

# HELPERS

# Generate a string to exclude possibly unwanted calendars
def buddy_cals(cals):
    return '-ec "' + ', '.join(cals) + '" '

# Generate a flag to pass options
def alf_pars():
    pars = tuple(str(arg) for arg in sys.argv[1:])
    if len(pars) >= 1:
        return pars[0]
    else:
        return ''

# MAIN

# Define
def main():

    global buddy_cmd, cal_excl, today_in

    # exclude calendars if available
    if cal_excl:
        buddy_cmd += buddy_cals(cal_excl)
    else:
        buddy_cmd += ''

    # include options if available
    options = alf_pars()

    # get results via iCalBuddy
    buddy_rez = ' '
    try:
        # if a number of forward days provided (including zero days)
        # show events and unfinished timed tasks
        options = int(options)
        if options == 0:
            buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡*"' + ' eventsToday+' + str(int(options)), shell = True)
            buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡!"' + ' tasksDueBefore:today+' + str(int(options)+1), shell = True)
        elif options > 0:
            if today_in:
                buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡*"' + ' eventsToday+' + str(int(options)), shell = True)
                buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡!"' + ' tasksDueBefore:today+' + str(int(options)+1), shell = True)
            else:
                _fr = str(datetime.date.today() + datetime.timedelta(days=1))
                _to = str(datetime.date.today() + datetime.timedelta(days=options))
                buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡*"' + " eventsFrom:'" + _fr + "' to:'" + _to + "'", shell = True)
                buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡!"' + ' tasksDueBefore:today+' + str(int(options)+1), shell = True)
    except ValueError:
        # if other command line arguments provided ('t' for 'tasks', no arguments for 'what is left for today')
        if options == 't':
        # show uncompleted tasks
            buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡!"' + ' undatedUncompletedTasks', shell = True)
        else:
            # show events left for today (default action if no command line arguments provided)
            # for that, introduce endpoints for listing events from right now till the end of the day
            _fr = str(datetime.datetime.today())[0:-7] + ' ' + gmt_shift
            _to = str(datetime.datetime.today())[0:-15] + '23:59:59 ' + gmt_shift
            buddy_rez += subprocess.check_output(buddy_cmd + '-b "¡*"' + " eventsFrom:'" + _fr + "' to:'" + _to + "'", shell = True)

    # convert a string containing the output to a list of separate events
    buddy_rez_lst = buddy_rez.split('¡')
    # escape some characters
    buddy_rez_lst = [el.replace('&', '&amp;') for el in buddy_rez_lst]
    # compress by removing empty elements, which appeared because of empty lines
    buddy_rez_lst = filter(None, buddy_rez_lst)
    # and convert a list of separate events to a list of lists of separate lines of each event
    for i in range(len(buddy_rez_lst)):
        buddy_rez_lst[i] = buddy_rez_lst[i].split('\n    ')

    # produce output
    print '<?xml version="1.0" encoding="utf-8"?>'
    print '<items>'
    for event in buddy_rez_lst:
        print '<item><title>' + event[0][1:] + '</title>'
        if event[0][0] == "*":
            print '<subtitle>' + event[-1][0:-1] + '</subtitle><icon>icl.png</icon></item>'
        else:
            if event[-1][0:3] == 'due':
                print '<subtitle>' + event[-1][0:-1] + '</subtitle><icon>rem.png</icon></item>'
            else:
                print '<icon>rem.png</icon></item>'
    print '</items>'
    print '</xmL>'

    return

# Run
if __name__ == "__main__":
    main()
